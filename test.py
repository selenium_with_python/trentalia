import imp
from operator import le
from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pdb
import time 
import datetime
driver = webdriver.Chrome('./chromedriver')
driver.get("https://www.trenitalia.com/")
driver.set_window_size(1920, 1080)
print("Trenitalia Website open successfully")
try:
    driver.execute_script("window.scrollTo(50,document.body.scrollHeight)")
    depature_station = driver.find_element_by_name("departureStation")
    depature_station.clear()
    depature_station.send_keys("Milano Centrale")
    print("Enter the depature Station Successfully")
    time.sleep(1)
    arrival_station = driver.find_element_by_name("arrivalStation")
    arrival_station.clear()
    arrival_station.send_keys("Roma ( Tutte Le Stazioni )")
    print("Enter the Arrival Station Successfully")
    time.sleep(1)
    try:
     search_btn = driver.find_element_by_xpath('//*[@id="sub-and-carnet"]/button')
     search_btn.click()
     print("Click on the search button successfully")
    except:
       print("Click Button is not find")
    element = WebDriverWait(driver,120).until(EC.presence_of_element_located(
   (By.ID, "header")))
    all_slots = element.find_elements_by_xpath("//*/solution")
    records = []
    for a in all_slots:
      time.sleep(3)  
      driver.execute_script("arguments[0].scrollIntoView();",a)
      try: 
        price_box = a.find_element_by_tag_name("div.price.text-primary.d-flex.justify-content-end a span")
        time.sleep(2)
        price_box.click()
      except:
        print("This slot does has price box")
      time.sleep(2)
      soup = BeautifulSoup(a.get_attribute('innerHTML'), 'html.parser')
      #trains = soup.select("span.train div div.body2")[0].text
      train_timing = soup.select("div.container div.col-2.time.text-nowrap")
      arrival_time = train_timing[0].text
      departure_time = train_timing[1].text
      duration = soup.select("div.container div.row div.duration")[0].text
      origin_station = soup.select("div.container div.row div.col-4.text-nowrap")[0].text
      destin_station = soup.select("div.col-3.offset-5.pl-0.text-nowrap")[0].text
      try:
       first_class =  soup.select("div.au-target.col.col-body")[0].text.strip().replace("  ","")  
      except:
         first_class = 0.0
      try:
       second_class = soup.select("div.au-target.col.col-body.cursor-pointer.active.ml-0 div")[0].text.strip().replace("  ","")
      except:
       second_class = 0.0
      try:
        time.sleep(2)
        price_box.click()
      except:
        print("Closing the price box if the slot is avaiable")
      arrival_connection = []
      departure_connection = []
      connections = []
      layover_connection = []
      try:
        # current_time = datetime.datetime.now() 
        # arrival_time = current_time.hour+":"+current_time.minute+":"+current_time.second
        connections_section  = a.find_element_by_tag_name("div.col-7.col-sm-12.pt-1.pr-0.d-flex div.d-flex a.d-none.d-sm-block.ml-2.au-target")
        time.sleep(3)
        connections_section.click()
        soup2 = BeautifulSoup(a.get_attribute('innerHTML'), 'html.parser')
        connection_arr = soup2.select("div.card-body.service-padding-bottom solution-stop")[0].text.split("\n")
        connection_arr =  [ item.replace("  ","") for item in connection_arr ]
        filter_object = filter(lambda x: x != "", connection_arr)
        arrival_connection = [item.replace("Arrivo:","") for item in filter_object if  "Arrivo:" in item]
        departure_connection = [item.replace("Partenza:","") for item in filter_object if  "Partenza:" in item]
        connections = [item.replace("Fermata:","") for item in filter_object if  ("Fermata:" in item) and (origin_station not in item) and (destin_station not in item)]
      except:
         print("Open the route of the train if this route is not short")
      result = {
        'origin_station' : origin_station,
        'destin_station' : destin_station,
        'arrival_time': arrival_time,
        'departure_time' : arrival_time ,
        'connections' : connections,
        'departure_connections' : departure_connection,
        'arrival_connections' : arrival_connection,
        'layover_connections' : layover_connection,
        'duration' : duration,
        'arrival_date' : "",
        'arrival_time' : arrival_time,
        'changes' : len(connections),
        'oneway' : "true",
        'direction' :"oneway",
        'operator' : "TRENTALIA",
        'price': {
            'first_class' : first_class,
            'second_class' : second_class
        },
      }
      print(result)        
finally:
   driver.close()
